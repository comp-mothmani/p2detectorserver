import re
import string


class Store:
    def __init__(self):
        self._cached_data = None
        self._name = None
        self.relativePath = None
        self.ws = None
        self._temperature = None

    def to_fahrenheit(self):
        return (self.temperature * 1.8) + 32

    @property
    def temperature(self):
        print("Getting value")
        return self._temperature

    @temperature.setter
    def temperature(self, value):
        if value < -273:
            raise ValueError("Temperature below -273 is not possible")
        print("Setting value")
        self._temperature = value


    @property
    def bar(self):
        if not self._cached_data:
            self._cached_data = self.readData()
        return self._cached_data

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        if not value:
            raise ValueError("Could not set any empty name.")
        self._name = value


    @staticmethod
    def readData():
        return None


st = Store()
st._temperature = -635
