import numpy as np

from sigProc.utils import delta, compute_distance, sec2hours


class SpeedFilter:
    def __init__(self):
        self.signal = []
        self.x_axis = []
        self.factors = []
        self.output = []

    def init_input_signal(self, gps):
        for i in xrange(len(gps) - 1):
            gp1 = gps[i]
            gp2 = gps[i + 1]

            t = delta(gp2["tstamp"], gp1["tstamp"])
            d = compute_distance(gp2["lat_val"], gp2["lon_val"], gp1["lat_val"], gp1["lon_val"])

            speed = (d * 1000) / (t) * 3.6
            self.signal.append(speed)  # 1m/s = 3.6km/h
            self.x_axis.append(gp1["tstamp"])

    def init_factors(self):
        pass

    def plot_input_signal(self):
        import matplotlib.pyplot as plt

        mean_velocity = sum(self.signal) / len(self.signal)
        mean_array = [mean_velocity for f in self.signal]
        plt.ylabel('Velcocity in Km/h' + "\n" + 'Mean velocity ' + str(int(mean_velocity)) + 'Km/h')
        plt.xlabel('TIMESTAMP in seconds')

        plt.plot(self.x_axis, self.signal)
        plt.plot(self.x_axis, mean_array)
        plt.show()

    def plot_profile(self):
        pass

    def plot(self):
        import matplotlib.pyplot as plt

        mean_velocity = sum(self.output) / len(self.output)
        mean_array = [mean_velocity for f in self.output]
        plt.ylabel('Filtered Velcocity in Km/h' + "\n" + 'Mean velocity ' + str(int(mean_velocity)) + 'Km/h')
        plt.xlabel('TIMESTAMP in seconds')

        plt.plot(self.x_axis, self.output)
        plt.plot(self.x_axis, mean_array)
        plt.show()

    def apply(self):
        speed_threshold = 5
        self.output = [f if f > speed_threshold else 0 for f in self.signal]


if __name__ == "__main__":
    import json

    fname = "/home/medzied/phole/adminApp/server/sigProc/data/Data-7176.json"
    with open(fname, "r") as f:
        js = json.load(f)
    gps = js["gpss"]

    flt = SpeedFilter()
    flt.init_input_signal(gps)
    flt.plot_input_signal()

    flt.apply()
    flt.plot()
