from math import sin, cos, sqrt, atan2, asin


def compute_distance(lat1, lon1, lat2, lon2):
    p = 0.017453292519943295    #Math.PI / 180
    a = 0.5 - cos((lat2 - lat1) * p)/2 + \
        cos(lat1 * p) * cos(lat2 * p) * \
        (1 - cos((lon2 - lon1) * p))/2

    return 12742 * asin(sqrt(a))  # 2 * R; R = 6371 km


def makeWindow(src, size, overlap=0):
    pass


def delta(a, b):
    import datetime
    ta = datetime.datetime.fromtimestamp(a / 1e3)
    tb = datetime.datetime.fromtimestamp(b / 1e3)
    tdelta = ta - tb
    return tdelta.total_seconds()

def sec2hours(s):
    return s/(60*60)
