import datetime
import json
from math import sin, cos, sqrt, atan2
import numpy as np
import matplotlib.pyplot as plt

import scipy.signal as sg
def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in xrange(0, len(l), n):
        yield l[i:i + n]

def autocorr(x, t=1):
    return np.corrcoef(np.array([x[:-t], x[t:]]))

def computeDistance(lat1, lon1, lat2, lon2):
    print lat1, lon1, lat2, lon2
    R = 6373.0
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = (sin(dlat / 2)) ** 2 + cos(lat1) * cos(lat2) * (sin(dlon / 2)) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    distance = R * c
    return distance
def computeLabelCoordinates(lbl,gpsPool):
    c = [i["tstamp"] - lbl for i in gpsPool]
    pos = [i if i > 0 else 9999999999 for i in c]
    neg = [i if i < 0 else -9999999999 for i in c]

    maxIndex = pos.index(min(pos))
    minIndex = neg.index(max(neg))

    a = gpsPool[maxIndex]
    b = gpsPool[minIndex]
    distanceAxB = computeDistance(a["lat_val"], a["lon_val"], b["lat_val"], b["lon_val"])

    ta = datetime.datetime.fromtimestamp(a["tstamp"]/ 1e3)
    tb = datetime.datetime.fromtimestamp(b["tstamp"]/ 1e3)
    ts = datetime.datetime.fromtimestamp(lbl/ 1e3)
    tdelta = ta - tb
    seconds = tdelta.total_seconds()

    tdeltas = ts - tb
    seconds2 = tdelta.total_seconds()

    meanVelocityKmh = distanceAxB/seconds
    factor = meanVelocityKmh*seconds2
    return (a["lat_val"] + factor*(b["lat_val"] - a["lat_val"]),
                          a["lon_val"] + factor*(b["lon_val"] - a["lon_val"]))
def sampleAround(lbl,accPool):
    d = []
    for i in accPool:
        if i["tstamp"] < lbl + 5000 and i["tstamp"] > lbl - 5000:
            d.append(i)
    return d


fname = "/home/medzied/phole/adminApp/server/sigProc/data/Data-7176.json"
with open(fname, "r") as f:
    js = json.load(f)

print js.keys()
gps = js["gpss"]
lbls = js["lbls"]
accs = js["accs"]
lbl = lbls[0]["tstamp"]
print computeLabelCoordinates(lbl,gps)
win = sampleAround(lbl,accs)
print win
print len(win)
x = []
y = []
for i in accs:
    x.append(i["tstamp"])
    y.append(i["x_val"])

plt.plot(x, y)

x = []
y = []
for i in accs:
    x.append(i["tstamp"])
    y.append(i["x_val"])

plt.plot(x, y)
for i in lbls:
    plt.plot(i["tstamp"], 0, "or")
plt.show()

w = []
for i in win:
    w.append(i["x_val"])
chnks = chunks(y,320)
m = []
for i in chnks:
    d = []
    for j in i:
        d.append(j)
    #print d
    try:
        z =  np.corrcoef(np.array([w,d]))[0][1]
        if abs(z) > 0.1:
            l = 1
        else:
            l = 0
        m.append(l)
    except:
        pass

import matplotlib.pyplot as plt
x = y

peaks, _ = sg.find_peaks_cwt(x, height=0)
plt.plot(x)
plt.plot(peaks, x[peaks], "x")
plt.plot(np.zeros_like(x), "--", color="gray")
plt.show()