import datetime
import json
import matplotlib.pyplot as plt
from math import sin, cos, sqrt, atan2

def computeDistance(lat1, lon1, lat2, lon2):
    print lat1, lon1, lat2, lon2
    R = 6373.0
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = (sin(dlat / 2)) ** 2 + cos(lat1) * cos(lat2) * (sin(dlon / 2)) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    distance = R * c
    return distance

fname = "/home/medzied/phole/adminApp/server/sigProc/data/Data-7176.json"
with open(fname, "r") as f:
    js = json.load(f)

gps = js["gpss"]
lbls = js["lbls"]
accs = js["accs"]
lbl = lbls[0]["tstamp"]


v = []
for i in xrange(len(gps)-1):
    gp1 = gps[i]
    gp2 = gps[i+1]

    d = computeDistance(gp1["lat_val"],gp1["lon_val"],gp2["lat_val"],gp2["lon_val"])
    print d
    ta = datetime.datetime.fromtimestamp(gp1["tstamp"]/ 1e3)
    tb = datetime.datetime.fromtimestamp(gp2["tstamp"]/ 1e3)
    tdelta = tb - ta
    seconds = tdelta.total_seconds()
    v.append(d/seconds)


plt.plot(xrange(len(v)),v)
plt.show()